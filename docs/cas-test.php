<?php

/*
 *Example from cardaccess
 */

?>
<!--
This script only demonstrates an online purchase with hash security

For other aspects of CASMTP (e.g. card present transactions, retrieving multiple
audit numbers for latency plagued links) please see the CASMTP manual.

This script is sample code only and omits significant error handling.
-->

<?php

$live_gateway_url = "https://etx.cardaccess.com.au/casmtp/casmtp.php";
$test_gateway_url = "https://etx.cardaccess.com.au/casmtp/testcasmtp.php";

###############################################################################
# Utility functions                                                           #
###############################################################################
function explode_kvps($lines, $line_sep, $kvp_sep)
{
	$resp = array ();
	$lines = explode ($line_sep, $lines);
	foreach ($lines as $line)
	{
		$kvp = explode ($kvp_sep, $line, 2);
		if (count ($kvp) != 2)
			continue;
		$key = $kvp[0];
		$value = $kvp[1];
		$resp[$key] = $value;
	}
	return $resp;
}

function DE048_kvp_encode_for_hash ($key, $value)
{
	return $key . urlencode ('=') . base64_encode ($value);
}

function DE048_kvp_encode_for_post ($key, $value)
{
	return $key . '=' . base64_encode ($value);
}

function DE048_encode_for_hash ($DE048)
{
	return array ("DE048" => implode (urlencode ('&'), array_map ('DE048_kvp_encode_for_hash', array_keys ($DE048), array_values ($DE048))));
}

function DE048_encode_for_post ($DE048)
{
	return array ("DE048" => implode ('&', array_map ('DE048_kvp_encode_for_post', array_keys ($DE048), array_values ($DE048))));
}

function casmtp_request($target_url, $postfields_without_DE048, $DE048, $hash_key)
{
	# POST fields for transaction
	$postfields_for_post = array_merge ($postfields_without_DE048, DE048_encode_for_post ($DE048));

	# Insert hashed fields
	insert_hash ($postfields_for_post, $postfields_without_DE048, $DE048, $hash_key);

	// Send POST
	$postdata = http_build_query ($postfields_for_post);
        error_log('Cas post data: ' . $postdata);
	$opts = array
	(
		'http' =>
			array
			(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
	);
	$context  = stream_context_create ($opts);
	$lines = file_get_contents ($target_url, false, $context);
        
        error_log($postdata);

	// Decode main key value pairs
	$resp = explode_kvps ($lines, "\n", "=");

	// Further decode DE048 which has its own key value pairs (also base 64 encoded)
	if (array_key_exists ("DE048", $resp))
	{
		$items = explode_kvps ($resp["DE048"], "&", "=");
		$resp["DE048"] = array ();
		foreach ($items as $key=>$value)
		{
			$resp["DE048"][$key] = base64_decode ($value);
		}
	}
	else
		$resp["DE048"] = array ();

	return $resp;
}

function get_kvp($key, $kvp_array, $default = "")
{
	//print_r ($key);
	//print_r ($kvp_array);
	if (!array_key_exists ($key, $kvp_array))
		return $default;
	else
		return $kvp_array[$key];
}

function get_DE048_kvp($key, $kvp_array)
{
	return get_kvp ($key, get_kvp ("DE048", $kvp_array, array ()));
}

function insert_hash (&$dest_fields, $post_fields, $DE048, $hash_key)
{
	$post_fields_for_hash = array_merge ($post_fields, DE048_encode_for_hash ($DE048));

	// Insert the current UTC timestamp
	$timestamp = gmdate ("Y/m/d G:i:s");
        $timestamp = "2012/03/23 12:41:48";
        
	$dest_fields["CAS_SECURITY_TIMESTAMP"] = $timestamp;
	$post_fields_for_hash["CAS_SECURITY_TIMESTAMP"] = $timestamp;
	// Make sure the post fields are appended in the correct order
	$request_keys = array_keys ($post_fields_for_hash);
	asort ($request_keys);
	// Concatenate all the post fields together
	$temp_str = "";
	foreach ($request_keys as $k)
	{
		$v = $post_fields_for_hash[$k];
		if ($temp_str != "")
			$temp_str .= "&";
		$temp_str .= $k . "=" . $v;
	}
	// Calculate the hash
        error_log("Cas hash string is: " . $temp_str);
	$hash_value = hash_hmac ("sha256", $temp_str, $hash_key);
	$dest_fields["CAS_SECURITY_TYPE"] = "Hash";
	$dest_fields["CAS_SECURITY_VALUE"] = $hash_value;
}

###############################################################################
# Configuration                                                               #
#                                                                             #
# This would normally be stored in a secure area on the website and not       #
# embedded in this script                                                     #
###############################################################################
# Which gateway to send this to
#
# IMPORTANT: To go live you must change the URL to the live URL
$use_live_gateway = FALSE;

# Which merchant ID to send this to
#
# IMPORTANT: Before going live you must change this to your assign CAS merchant
# id
$merchant_id = "917";

# Which hash key to use
$hash_key = "hfreu374r74rhg3irel;o439u483u483i4j3;p390";

###############################################################################
# Step 1. Derive configuration values                                         #
###############################################################################
$target_url = $use_live_gateway? $live_gateway_url: $test_gateway_url;
$formatted_merchant_id = str_pad($merchant_id, 15, "0", STR_PAD_LEFT);

?><br>Step 1. Sending transaction to: <b><?=$target_url?></b> for merchant <b><?=$merchant_id?></b>

<?
##############################################
#### Step 2. Retrieve unique audit number ###
##############################################
?><p>--- Step 2. Audit request ---<?
$resp = casmtp_request
(
	$target_url,
	array
	(
		'DE001' => '0800',
		'dataformat' => 'HTTP_AS2805',
		'DE042' => $formatted_merchant_id
	),
	array
	(
		'CAS.REQUEST.AUDIT' => '1'
	),
	$hash_key
);

$audit = get_DE048_kvp ("CAS.RESPONSE.AUDIT1", $resp);
if ($audit == "" or $audit == "-1")
{
	?><br><b>ERROR: Couldn't get audit number</b><?
	exit;
}


?><br>Audit = <b><?=$audit?></b> (please quote when tracing a transaction)<?

#################################
### Step 3. Make purchase     ###
#################################
?><p>--- Step 3. Make purchase ---<?
$resp = casmtp_request
(
	$target_url,
	array
	(
		'dataformat' => 'HTTP_AS2805',

		# Purchase method	
		'DE001' => '0200',
		'DE003' => '003000',

		'DE042' => $formatted_merchant_id,

		# $1.00 transaction
		'DE004' => '2000',

		# Card Number 4 000 000 000 000 00 2
		//'DE002' => '4000000000000002',
                  'DE002' => '4111111111111111',

		# Expiry YYMM (NB: Reverse of what is usually displayed on a credit card) YY=(20)12 MM=08 in the example below
		'DE014' => '1302'
	),
	# KVPs: Audit number (required), customer reference (optional), cvc (optional), ...
	# Note that these are all individually base 64 encoded
	array
	(
		'CAS.AUDIT' => $audit,
		'CAS.CUSTREF' => '15',	# Optional
		'CAS.CARD.CVC' => '123',

		# Any extra KVPs to be sent can also be embedded here
	//	'cardholder_name' => 'JohnDoe',
	//	'cardholder_address' => '5 Somewhere Street'
	),
	$hash_key
);

$resp_code = get_kvp ("DE039", $resp);
$setl_date = get_kvp ("DE015", $resp);
$auth_code = get_kvp ("DE038", $resp);
$status_code = get_DE048_kvp ("CAS.RESPONSE.STATUSCODE", $resp);

?><br>Status code = <b><?=$status_code?></b><?
if ($status_code == "0")
{
	// Definitely approved or declined
	?><br>Bank Response code = <b><?=$resp_code?></b><?
	?><br>Bank Settlement Date (YYMM) = <b><?=$setl_date?></b><?
	?><br>Bank Auth Code = <b><?=$auth_code?></b><?
	if (in_array ($resp_code, array ("00", "08", "10", "11", "16", "77")))
	{
		?><br><b>Approved</b><?
	}
	else
	{
		?><br><b>Declined</b><?
	}
}
else if ($status_code == "17")
{
	?><br><b>Timeout - manual check required</b><?
}
else if ($status_code == "19")
{
	?><br><b>Transaction denied by gateway</b><?
}
else if ($status_code == "15")
{
	// This is in case the same transaction against the same audit number is sent again and the transaction is still going. You will almost never see this in practice
	?><br><b>Transaction already in progress</b><?
}
else
{
	// Check with Card Access Services for a full list
	?><br><b>Other status code</b><?
}
?>


