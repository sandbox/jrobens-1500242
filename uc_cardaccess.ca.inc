<?php

/**
 * @file uc_cardaccess.ca.inc
 * This file contains the ubercart Conditional Action hooks and functions.
 */
/* * ****************************************************************************
 * Conditional Action Hooks                                                   *
 * **************************************************************************** */

/**
 * Implementation of hook_ca_trigger().
 */
function uc_cardaccess_ca_trigger() {
    $triggers['uc_cardaccess_payment_processed'] = array(
        '#title' => t('A payment gets successfully processed for an order'),
        '#category' => t('CardAccess'),
        '#arguments' => array(
            'order' => array(
                '#entity' => 'uc_order',
                '#title' => t('Order'),
            ),
        ),
    );

    return $triggers;
}

/**
 * Implementation of hook_ca_predicate().
 */
function uc_cardaccess_ca_predicate() {
    $predicates['uc_cardaccess_payment_processed'] = array(
        '#title' => t('Successful payment via CardAccess'),
        '#trigger' => 'uc_cardaccess_payment_processed',
        '#class' => 'uc_cardaccess',
        '#status' => variable_get('uc_cardaccess_change_order_status', UC_CARDACCESS_CHANGE_ORDER_STATUS_DEFAULT),
        '#actions' => array(
            array(
                '#name' => 'uc_order_action_update_status',
                '#title' => t('Update status of order'),
                '#argument_map' => array(
                    'order' => 'order',
                ),
            ),
        ),
        '#conditions' => array(
            '#operator' => 'AND',
            '#conditions' => array(
                array(
                    '#name' => 'uc_payment_condition_order_balance',
                    '#title' => t('If the balance is less than or equal to $0.00.'),
                    '#argument_map' => array(
                        'order' => 'order',
                    ),
                    '#settings' => array(
                        'negate' => FALSE,
                        'balance_comparison' => 'less_equal',
                    ),
                ),
                array(
                    '#name' => 'uc_order_status_condition',
                    '#title' => t('If the order status is not already Payment Received.'),
                    '#argument_map' => array(
                        'order' => 'order',
                    ),
                    '#settings' => array(
                        'negate' => TRUE,
                        'order_status' => 'payment_received',
                    ),
                ),
            ),
        ),
        '#settings' => array(
            'order_status' => 'payment_received',
        ),
    );

    return $predicates;
}
