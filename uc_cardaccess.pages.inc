<?php

/**
 * @file
 * Includes page callbacks for user specific recurring fee operation pages.
 * 
 * jrobens@interlated.com.au 201203
 */

/**
 * Displays a form for customers to update their CC info.
 */
function uc_cardaccess_token_update_form(&$form_state, $rfid) {
    $form = array();

    $form['rfid'] = array(
        '#type' => 'value',
        '#value' => $rfid,
    );
    $form['cc_data'] = array(
        '#type' => 'fieldset',
        '#title' => t('Credit card details'),
        '#theme' => 'uc_payment_method_credit_form',
        '#tree' => TRUE,
    );
    $form['cc_data'] += uc_payment_method_credit_form(array(), $order);
    unset($form['cc_data']['cc_policy']);

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Update'),
        '#suffix' => l(t('Cancel'), 'user/' . $user->uid),
    );

    return $form;
}

/**
 * Submit handler for the token update form.
 */
function uc_cardaccess_token_update_form_submit($form, &$form_state) {
    $fee = uc_recurring_fee_user_load($form_state['values']['rfid']);
    $order = uc_order_load($fee->order_id);
    $order->payment_details = $form_state['values']['cc_data'];

    $result = uc_cardaccess_token_update($order, $fee);

    // If the update was successful.
    if ($result) {
        drupal_set_message(t('The payment details for that recurring fee have been updated.'));
    } 
    else {
        drupal_set_message(t('An error has occurred while updating your payment details. Please try again and contact us if you are unable to perform the update.'), 'error');
    }
}
